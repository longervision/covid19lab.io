function btn_cases_update( num ) {

    document.getElementById('btn_cases').textContent = num.toLocaleString();
    document.getElementById('btn_cases').setAttribute('data-cases', num);

}

function btn_recovered_update( num ) {

    document.getElementById('btn_recovered').textContent = num.toLocaleString();
    document.getElementById('btn_recovered').setAttribute('data-recovered', num);

    document.getElementById('btn_closedRecovered').textContent = num.toLocaleString();
    document.getElementById('btn_closedRecovered').setAttribute('data-recovered', num);

}

function btn_deaths_update( num ) {

    document.getElementById('btn_deaths').textContent = num.toLocaleString();
    document.getElementById('btn_deaths').setAttribute('data-deaths', num);

    document.getElementById('btn_closedDeaths').textContent = num.toLocaleString();
    document.getElementById('btn_closedDeaths').setAttribute('data-closedDeaths', num);

}

function btn_closedCases_update( num ) {

    document.getElementById('btn_closedCases').textContent = num.toLocaleString();
    document.getElementById('btn_closedCases').setAttribute('data-closedCases', num);

}

function info_recoveredPercentage_update( num ) {

    document.getElementById('info_recoveredPercentage').textContent = num + '%';
    document.getElementById('info_recoveredPercentage').setAttribute('data-recoveredPercentage', num);

}

function info_deathsPercentage_update( num ) {

    document.getElementById('info_deathsPercentage').textContent = num + '%';
    document.getElementById('info_deathsPercentage').setAttribute('data-deathsPercentage', num);

}

function btn_countriesNum_update( num ) {

    document.getElementById('btn_countriesNum').textContent = num;
    document.getElementById('btn_countriesNum').setAttribute('data-countriesNum', num);

}

function btn_activeCases_update( num ) {

    document.getElementById('btn_activeCases').textContent = num.toLocaleString();
    document.getElementById('btn_activeCases').setAttribute('data-activeCases', num);

}

function populateCounters( response ) {

    var cases         = 0;
    var recovered     = 0;
    var deaths        = 0;
    var closedCases   = 0;
    var recoveredPerc = 0;
    var deathsPerc    = 0;
    var countries     = 0;
    var activeCases   = 0;
    var fatality      = 0;

    for ( var country in response ) {

        cases       = cases       + response[ country ].confirmed;
        recovered   = recovered   + response[ country ].recovered;
        deaths      = deaths   + response[ country ].deaths;
        countries   = countries + 1;

    }

    closedCases     = recovered + deaths;
    recoveredPerc   = (( recovered * 100 ) / closedCases ).toFixed(0);
    deathsPerc      = 100 - recoveredPerc;
    activeCases     = cases - closedCases;
    fatality        = ( ( deaths / cases ) * 100 ).toFixed( 1 );

    btn_cases_update( cases );
    btn_recovered_update( recovered );
    btn_deaths_update( deaths );
    btn_closedCases_update( closedCases );
    info_recoveredPercentage_update( recoveredPerc );
    info_deathsPercentage_update( deathsPerc );
    btn_countriesNum_update( countries );
    btn_activeCases_update( activeCases );
    document.getElementById( 'btn_fatalityRate' ).textContent           = fatality + '%';

}

function populateCountryCounters( country ) {

    graphButtonsReset();

    createGraphData( graphData, 'confirmed', data[ country ].g );

    var flipcards     = document.querySelectorAll( '.flipCard' );
    var flipcardsNum  = flipcards.length;

    var fatalityRate          = ( ( data[ country ].deaths / data[ country ].confirmed ) * 100 ).toFixed( 1 );
    var closedCases           = data[ country ].recovered + data[ country ].deaths;
    var activeCases           = data[ country ].confirmed - closedCases;

    var recoveredPercentage   = (( data[ country ].recovered * 100 ) / closedCases ).toFixed(0);

    if ( isNaN( recoveredPercentage ) ) {

        recoveredPercentage = 0;
        deathsPercentage = 0;

    } else {

        var deathsPercentage      = 100 - recoveredPercentage;

    }

    // console.log( recoveredPercentage );

    document.getElementById( 'country_name' ).textContent                       = translateCountry( country );
    document.getElementById( 'country_btn_cases' ).textContent                  = data[ country ].confirmed;
    document.getElementById( 'country_btn_deaths' ).textContent                 = data[ country ].deaths;
    document.getElementById( 'country_btn_recovered' ).textContent              = data[ country ].recovered;
    document.getElementById( 'country_btn_newCases' ).textContent               = '+' + data[ country ].newcases;
    document.getElementById( 'country_btn_newDeaths' ).textContent              = '+' + data[ country ].newdeaths;
    document.getElementById( 'country_btn_fatalityRate' ).textContent           = fatalityRate + '%';
    document.getElementById( 'country_btn_closedRecovered' ).textContent        = data[ country ].recovered;
    document.getElementById( 'country_info_recoveredPercentage' ).textContent   = recoveredPercentage + '%';
    document.getElementById( 'country_btn_closedDeaths' ).textContent           = data[ country ].deaths;
    document.getElementById( 'country_info_deathsPercentage' ).textContent      = deathsPercentage + '%';
    document.getElementById( 'country_btn_closedCases' ).textContent            = closedCases;
    document.getElementById( 'country_btn_activeCases' ).textContent            = activeCases;

    for ( var i = 0 ; i < flipcardsNum ; i++ ) {

        flipcards[ i ].classList.add('flipActive');

    }

}

function unPopulateCountryCounters() {

    graphButtonsReset();

    createGraphData( graphData, 'confirmed', 'world' );

    var flipcards     = document.querySelectorAll( '.flipCard' );
    var flipcardsNum  = flipcards.length;

    for ( var i = 0 ; i < flipcardsNum ; i++ ) {

        flipcards[ i ].classList.remove('flipActive');

    }

}

populateCounters( data );
